package com.kosign.dev.utils;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.TimeZone;

public class DateTimeUtils {
    private static final String VA_PATTERN_DATE8 = "yyyyMMdd";
    public static final DateTimeFormatter VA_FORMATTER_DATE8 = DateTimeFormatter.ofPattern(VA_PATTERN_DATE8);
    public static final Clock clock = Clock.system(TimeZone.getDefault().toZoneId());
    public static LocalDateTime ictNow() {
        return LocalDateTime.now(clock);
    }
    public static String getTimeNow(){
        return ictNow().format(VA_FORMATTER_DATE8);
    }
}
