package com.kosign.dev.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import com.kosign.dev.global.converters.AbstractEnumConverter;

/**
 * A class can be used for getting DCertificateType enum
 */
public enum ProductType implements GenericEnum<ProductType, String> {
    STOCK("1"),
    SOLD_OUT("2"),
    NEW("3"),
    ;

    private final String value;

    ProductType(String value) {
        this.value = value;
    }

    /**
     * Method fromValue : Check Enum value
     *
     * @param value value that have to check
     * @return enum value
     */
    @JsonCreator
    public static ProductType fromValue(String value) {
        for(ProductType my: ProductType.values()) {
            if(my.value.equals(value)) {
                return my;
            }
        }

        return null;
    }

    /**
     * Method getValue : Get Enum value
     * @return Enum value
     */
    @JsonValue
    public String getValue() {
        return value;
    }

    /**
     * Method getLabel : Get Enum Label
     * @return Enum Label
     */
    @Override
    public String getLabel() {
        String label = "(no label)";

        if("1".equals(value)) label = "IN STOCK";
        else if("2".equals(value)) label = "SOLD OUT";
        else if("3".equals(value)) label = "NEW";

        return label;
    }

    public static class Converter extends AbstractEnumConverter<ProductType, String> {

        public Converter() {
            super(ProductType.class);
        }

    }

}
