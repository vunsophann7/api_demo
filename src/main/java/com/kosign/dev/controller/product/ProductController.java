package com.kosign.dev.controller.product;

import com.kosign.dev.controller.AbstractRestController;
import com.kosign.dev.global.MultiSortBuilder;
import com.kosign.dev.payload.product.ProductCriteria;
import com.kosign.dev.payload.product.ProductRequest;
import com.kosign.dev.service.product.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/product")
public class ProductController extends AbstractRestController {

    private final ProductService productService;

    @PostMapping
    public Object createProduct(@RequestBody ProductRequest payload) {
        productService.createProduct(payload);
        return ok();
    }

    @GetMapping
    public ResponseEntity<?> getProductList(
            @RequestParam(name = "sort_columns", required = false, defaultValue = "name:asc") String sortColumns,
            @RequestParam(value = "page_number", defaultValue = "0") int page,
            @RequestParam(value = "page_size", defaultValue = "10") int size
    ) {
        List<Sort.Order> sortBuilder = new MultiSortBuilder().with(sortColumns).build();
        Pageable pageable = PageRequest.of(page, size, Sort.by(sortBuilder));
        ProductCriteria criteria = ProductCriteria.builder()
                .sortColumns(sortColumns)
                .pageNumber(page)
                .pageSize(size)
                .build();
        return ok(productService.getProductList(criteria, pageable));
    }

    @GetMapping("/{id}")
    public Object getById(@PathVariable Long id) {
        return ok(productService.getById(id));
    }

    @PutMapping("/{id}")
    public Object updateProduct(@PathVariable Long id, @RequestBody ProductRequest payload) {
        productService.updateProduct(id, payload);
        return ok();
    }

    @DeleteMapping("/{id}")
    public Object deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
        return ok();
    }
}
