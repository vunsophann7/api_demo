package com.kosign.dev.controller.category;

import com.kosign.dev.controller.AbstractRestController;
import com.kosign.dev.payload.category.CategoryRequest;
import com.kosign.dev.service.category.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/category")
@RequiredArgsConstructor
public class CategoryController extends AbstractRestController {
     private final CategoryService categoryService;

     @PostMapping
     public Object createCategory(@RequestBody CategoryRequest payload) {
         categoryService.createCategory(payload);
         return ok();
     }

     @GetMapping
     public Object getAllCategories() {
         return ok(categoryService.getAllCategories());
     }

    @DeleteMapping("/{id}")
    public Object deleteCategory(@PathVariable Long id) {
        categoryService.deleteCategory(id);
        return ok();
    }
}
