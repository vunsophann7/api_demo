package com.kosign.dev.service.product;

import com.kosign.dev.payload.product.ProductCriteria;
import com.kosign.dev.payload.product.ProductRequest;
import org.springframework.data.domain.Pageable;
public interface ProductService {
    void createProduct(ProductRequest payload);

    Object getProductList(ProductCriteria productCriteria, Pageable pageable);

    Object getById(Long id);

    void updateProduct(Long id, ProductRequest payload);

    void deleteProduct(Long id);
}
