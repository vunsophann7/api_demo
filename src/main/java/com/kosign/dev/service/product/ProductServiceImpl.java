package com.kosign.dev.service.product;

import com.kosign.dev.common.api.StatusCode;
import com.kosign.dev.domain.category.Category;
import com.kosign.dev.domain.category.CategoryRepository;
import com.kosign.dev.domain.product.Product;
import com.kosign.dev.domain.product.ProductRepository;
import com.kosign.dev.enums.ProductType;
import com.kosign.dev.enums.Status;
import com.kosign.dev.exception.BusinessException;
import com.kosign.dev.global.MultiSortBuilder;
import com.kosign.dev.payload.product.ProductCriteria;
import com.kosign.dev.payload.product.ProductMainResponse;
import com.kosign.dev.payload.product.ProductRequest;
import com.kosign.dev.payload.product.ProductResponse;
import com.kosign.dev.utils.DateTimeUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {
    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    @Override
    public void createProduct(ProductRequest payload) {
        Category category = categoryRepository.findById(payload.getCategory())
                .orElseThrow(() -> new BusinessException(StatusCode.NOT_FOUND));
        Product product = Product.builder()
                .name(payload.getName())
                .description(payload.getDescription())
                .price(payload.getPrice())
                .proDate(DateTimeUtils.getTimeNow())
                .productType(ProductType.NEW)
                .status(Status.NORMAL)
                .category(category)
                .build();
        productRepository.save(product);
    }

    @Override
    public Object getProductList(ProductCriteria productCriteria, Pageable pageable) {
        Page<Product> productList = productRepository.findAllProduct(pageable);
        List<ProductResponse> productResponses = productList.stream()
                .map(product -> ProductResponse.builder()
                        .id(product.getId())
                        .name(product.getName())
                        .description(product.getDescription())
                        .price(product.getPrice())
                        .proDate(product.getProDate())
                        .productType(product.getProductType())
                        .category(product.getCategory())
                        .build())
                .toList();
        return ProductMainResponse.builder()
                .products(productResponses)
                .pagination(productList)
                .build();
    }

    @Override
    public Object getById(Long id) {
        var product = productRepository.findById(id)
                .orElseThrow(() -> new BusinessException(StatusCode.NOT_FOUND));
        return ProductResponse.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .proDate(product.getProDate())
                .productType(product.getProductType())
                .category(product.getCategory())
                .build();
    }
    @Override
    public void updateProduct(Long id, ProductRequest payload) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new BusinessException(StatusCode.NOT_FOUND));

        // Only update the name if a new name is provided
        if (payload.getName() != null) {
            product.setName(payload.getName());
        }

        // Only update the description if a new description is provided
        if (payload.getDescription() != null) {
            product.setDescription(payload.getDescription());
        }

        // Only update the price if a new price is provided
        if (payload.getPrice() != null) {
            product.setPrice(payload.getPrice());
        }

        // Only update the category if a new category is provided
        if (payload.getCategory() != null) {
            Category category = categoryRepository.findById(payload.getCategory())
                    .orElseThrow(() -> new BusinessException(StatusCode.NOT_FOUND));
            product.setCategory(category);
        }

        productRepository.save(product);
    }

    @Override
    public void deleteProduct(Long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new BusinessException(StatusCode.NOT_FOUND));
        product.setStatus(Status.DISABLE);
        productRepository.save(product);
    }
}
