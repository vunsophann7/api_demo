package com.kosign.dev.service.category;

import com.kosign.dev.common.api.StatusCode;
import com.kosign.dev.domain.category.Category;
import com.kosign.dev.domain.category.CategoryRepository;
import com.kosign.dev.enums.Status;
import com.kosign.dev.exception.BusinessException;
import com.kosign.dev.payload.category.CategoryRequest;
import com.kosign.dev.payload.category.CategoryResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    @Override
    public void createCategory(CategoryRequest categoryRequest) {
        Category category = Category.builder()
                .name(categoryRequest.getName())
                .description(categoryRequest.getDescription())
                .status(Status.NORMAL)
                .build();
        categoryRepository.save(category);
    }

    @Override
    public Object getAllCategories() {
        var categories = categoryRepository.findAllByStatus();
        List<CategoryResponse> categoryResponses = categories.stream()
                .map(category -> CategoryResponse.builder()
                        .id(category.getId())
                        .name(category.getName())
                        .description(category.getDescription())
                        .build())
                .toList();
        return categoryResponses;
    }

    @Override
    public void deleteCategory(Long id) {
        var cateId = categoryRepository.findById(id)
                .orElseThrow(() -> new BusinessException(StatusCode.NOT_FOUND));
        cateId.setStatus(Status.DISABLE);
        categoryRepository.save(cateId);
    }
}
