package com.kosign.dev.service.category;

import com.kosign.dev.payload.category.CategoryRequest;

public interface CategoryService {
    void createCategory(CategoryRequest categoryRequest);
    Object getAllCategories();
    void deleteCategory(Long id);
}
