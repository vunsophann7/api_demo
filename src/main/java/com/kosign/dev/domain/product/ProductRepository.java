package com.kosign.dev.domain.product;

import com.kosign.dev.payload.product.IProductResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("SELECT p FROM Product p where p.status = '1'")
    Page<Product> findAllProduct(Pageable pageable);

//    @Query(value = """
//            select p.id, p.nm as name, p.des as description, p.price as price, tc.id, tc.nm as name from tb_product p
//            join tb_category tc on tc.id = p.category_id
//            where p.sts = '1'
//    """, nativeQuery = true)
//    Page<IProductResponse> findAllProducts(Pageable pageable);

}
