package com.kosign.dev.domain.product;

import com.kosign.dev.constant.Constants;
import com.kosign.dev.domain.category.Category;
import com.kosign.dev.enums.ProductType;
import com.kosign.dev.enums.Status;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.JdbcTypeCode;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "tb_product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nm")
    private String name;
    @Column(name = "des")
    private String description;
    @Column(name = "price")
    private BigDecimal price;
    @Column(name = "pro_dt", nullable = false, length = Constants.DATE_LENGTH)
    private String proDate;

    @Column(name = "pro_type", length = 1)
    @ColumnDefault("'1'")
    @Convert(converter = ProductType.Converter.class)
    private ProductType productType;

    @Column(name = "sts",nullable = false, length = Types.CHAR)
    @JdbcTypeCode(Types.CHAR)
    @Convert(converter = Status.Converter.class)
    private Status status;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Builder
    public Product(String name, String description, BigDecimal price, String proDate, ProductType productType, Status status, Category category) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.proDate = proDate;
        this.productType = productType;
        this.status = status;
        this.category = category;
    }
}
