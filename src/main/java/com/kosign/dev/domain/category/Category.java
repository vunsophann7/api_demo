package com.kosign.dev.domain.category;

import com.kosign.dev.enums.Status;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.JdbcTypeCode;

import java.sql.Types;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tb_category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nm")
    private String name;
    @Column(name = "des")
    private String description;
    @Column(name = "sts", length = Types.CHAR)
    @JdbcTypeCode(Types.CHAR)
    @Convert(converter = Status.Converter.class)
    private Status status;
    @Builder
    public Category(String name, String description, Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }
}
