package com.kosign.dev.payload.product;

import com.kosign.dev.enums.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class ProductRequest {
    private String name;
    private String description;
    private BigDecimal price;
    private String proDate;
    private Integer productType;
    private Long category;

}
