package com.kosign.dev.payload.product;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ProductCriteria {

        private String searchValue;
        private String sortColumns;
        private String status;
        private String type;
        private Integer pageNumber;
        private Integer pageSize;

        @Builder
        public ProductCriteria(String searchValue,String sortColumns, String status, String type, Integer pageNumber, Integer pageSize) {
            this.searchValue = searchValue;
            this.sortColumns = sortColumns;
            this.status = status;
            this.type = type;
            this.pageNumber = pageNumber;
            this.pageSize = pageSize;
        }
}
