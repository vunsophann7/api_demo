package com.kosign.dev.payload.product;

import com.kosign.dev.domain.category.Category;
import org.springframework.beans.factory.annotation.Value;

public interface IProductResponse {
    @Value("#{target.id}")
    Long getId();
    @Value("#{target.name}")
    String getName();
    @Value("#{target.description}")
    String getDescription();
    @Value("#{target.price}")
    String getPrice();
    @Value("#{target.category}")
    Category getCategory();

}
