package com.kosign.dev.payload.product;

import com.kosign.dev.common.Pagination;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class ProductMainResponse {
    private List<ProductResponse> products;
    private Pagination pagination;

    @Builder
    public ProductMainResponse(List<ProductResponse> products, Page<?> pagination) {
        this.products = products;
        this.pagination = new Pagination(pagination);
    }
}
