package com.kosign.dev.payload.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.kosign.dev.domain.category.Category;
import com.kosign.dev.enums.ProductType;
import com.kosign.dev.enums.Status;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class ProductResponse {
    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private String proDate;
    private ProductType productType;

    private Category category;

    @Builder
    public ProductResponse(Long id, String name, String description, BigDecimal price, String proDate,ProductType productType, Category category) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.proDate = proDate;
        this.productType = productType;
        this.category = category;
    }
}
