package com.kosign.dev.payload.user;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UserRequest {
    private String email;
    private String password;
    private String role;

    @Builder
    public UserRequest(String email, String password, String role) {
        this.email = email;
        this.password = password;
        this.role = role;
    }
}
